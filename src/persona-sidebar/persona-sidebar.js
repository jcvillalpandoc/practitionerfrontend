import { LitElement, html } from 'lit-element';  
class PersonaSidebar extends LitElement {
	static get properties() {
		return {			
	};
}

	constructor() {
		super();			
	}

	render() {
		return html`		
			<!-- Enlace Bootstrap -->
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
			<aside>
				<section>				
					<div class="mt-5">
						<button @click="${this.newPerson}" class="w-100 btn bg-success" style="font-size: 50px"><strong>+</strong></button>
                    </div>				
				</section>
			</aside>
		`;
	}    

    newPerson(e) {
        console.log("newPerson en persona-sidebar");
        console.log("Se va a crear una nueva persona");
      
        this.dispatchEvent(new CustomEvent("new-person", {})); 
    }
}  
customElements.define('persona-sidebar', PersonaSidebar)